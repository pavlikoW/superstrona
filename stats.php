<?php
  require_once 'login.php';
  $conn = new mysqli($hn, $un, $pw, $db);
  if ($conn->connect_error) die($conn->connect_error);
  $query = "SELECT * FROM boardgames, gamesplayed WHERE boardgames.title=gamesplayed.title";
  $result = $conn->query($query);
  if (!$result) die ("Brak dostępu do bazy danych: " . $conn->error);
  $rows = $result->num_rows;

echo <<<_END
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content ="width=device-width">
    <meta name="author" content="Pavliko">
    <title>BoardgameDiary | Welcome</title>
    <link rel="stylesheet" href="./css/style.css">
    <script src="js/jquery-3.2.1.min.js"></script>
    <script src="js/script.js"></script>
  </head>
  <body>
    <header>
      <div class="container">
        <div id="logo">
          <h1>Board<span class="highlight">game</span>Diary</h1>
        </div>
        <nav>
            <a href="index.php">Home</a>
            <a href="diary.php">Diary</a>
            <a class="current" href="stats.php">Stats</a>
        </nav>
      </div>
    </header>
    <section id="main">
      <div class="container">
          <article id="main-col">
            <h1 class="page-title"><span>Games you played so far...</span></h1>
          </article>
          <div class="games">
_END;
          for($j=0; $j<$rows;++$j){
          echo  '<div class="game">';
              $result->data_seek($j);
              $row = $result->fetch_array(MYSQLI_ASSOC);
              echo '<div class="gameimage">';
              echo '<img src="' .$row[img].'">';
              echo '</div>';
              echo '<div class="content">';
              echo '
               <h2><span>'.$row[title] .'</span></h2>
               <h3>Author: <span>' .$row[author] .'</span></h3>
               <h3>Category: <span>' .$row[category] .'</span></h3>
               <h3>Printing: <span>' .$row[printing].'</span></h3>
               <h3>Games count: <span>' .$row[count].'</span></h3>
               <h3>Best score: <span>' .$row[bestscore].'</span></h3>
               ';
              echo '</div>';
          echo '</div>';

          };
?>
        </div>
    </div>

    </section>
    <footer>
      <p>Boardgame Diary, Copyright &copy; 2018</p>
    </footer>
  </body>
</html>
<?p
$result->close();
$conn->close();
?>
