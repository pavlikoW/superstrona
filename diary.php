<?php

  require_once 'login.php';
  $conn = new mysqli($hn, $un, $pw, $db);
  if ($conn->connect_error) die($conn->connect_error);
  if (isset($_POST['delete']) && isset($_POST ['idlogs'])&& isset($_POST['id'])){

    $idlogs = get_post($conn, 'idlogs');
    $id = get_post($conn,'id');
    $query = "DELETE FROM diarylogs WHERE idlogs='$idlogs'";
    $result = $conn->query($query);
    $query = "DELETE FROM boardgames WHERE id='$id'";
    $result = $conn->query($query);

  }

  if (isset($_POST['title'])
      && isset($_POST['author'])
      && isset($_POST['category'])
      && isset($_POST['printing'])
      && isset($_POST['bestscore'])
      && isset($_POST['comment'])
      && isset($_POST['date']))
      {
        $title = get_post($conn, 'title');
        $author = get_post($conn, 'author');
        $category = get_post($conn, 'category');
        $bestscore = get_post($conn, 'bestscore');
        $printing = get_post($conn, 'printing');
        $comment = get_post($conn, 'comment');
        $date = get_post($conn, 'date');

        $query = "INSERT INTO boardgames VALUES".
        "(NULL,'$title', '$author', '$category', $printing,default) ";
        $result= $conn->query($query);
        if (!$result) echo "Instrukcja INSERT nie powiodła się: $query<br>" .$conn->error . "<br><br>";

        $query = "INSERT INTO diarylogs VALUES".
        "(NULL,'$date', '$bestscore', '$title', '$comment') ";
        $result= $conn->query($query);
        if (!$result) echo "Instrukcja INSERT nie powiodła się: $query<br>" .$conn->error . "<br><br>";


      }

 ?>

<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content ="width=device-width">
    <meta name="author" content="Pavliko">
    <title>BoardgameDiary | Welcome</title>
    <link rel="stylesheet" href="./css/style.css">
    <script src="js/jquery-3.2.1.min.js"></script>
    <script src="js/script.js"></script>
  </head>
  <body>
    <header>
      <div class="container">
        <div id="logo">
          <h1>Board<span class="highlight">game</span>Diary</h1>
        </div>
        <nav>
            <a href="index.php">Home</a>
            <a class="current" href="diary.php">Diary</a>
            <a href="stats.php">Stats</a>
        </nav>
      </div>
    </header>
    <section id="main-diary">
      <div class="container">
        <aside class="addnewgame">
          <button class="btn btn-add" ><img src="./img/add.png" height="25px">Add new game</button>
          <form id="form-addgame" action="diary.php" method="post">
                Title <input type="text" name="title" value="" >
                Author <input type="text" name="author" value="">
                Category <input type="text" name="category" value="">
                Printing <input type="text" name="printing" value="">
                Bestscore <input type="text" name="bestscore" value="">
                Date <input type="date" name="date" value="">
                Comment <input type="text" name="comment" height="40px" value="">
                <input class="btn" type="submit" name="addpicture" value="Add picture">
                <input class="btn" type="submit" name="addgame" value="Add game">
          </form>
        </aside>
        <div id="main-col">
          <h1><span>Last games:</span></h1>
          <?php
          $query= "SELECT * FROM diarylogs JOIN boardgames ON diarylogs.title=boardgames.title ORDER BY dates DESC";
          $result = $conn->query($query);
          $rows= $result->num_rows;


          for($j=0; $j<$rows; ++$j){
              echo
              '<div class="diarylogs">';
              $result -> data_seek($j);
              $row = $result->fetch_array(MYSQLI_ASSOC);
              echo
              '<img src="' .$row[img].'">
                <div id=diaryinfo>
                  <h3>Title: <span>' .$row[title] .'</span></h3>
                  <h3>Author: <span>' .$row[author] .'</span></h3>
                  <h3>Score: <span>' .$row[score] .'</span></h3>
                  <h3>Date: <span>' .$row[dates].'</span></h3>
                </div>
                <form action="diary.php" method="post">
                <input type="hidden" name="delete" value="yes">
                <input type="hidden" name="idlogs" value="'.$row[idlogs] .'">
                <input type="hidden" name="id" value="'.$row[id] .'">
                <input type="submit" class="btn btnx" value="X"></form>
              </div>
              <p id="last">Comment: <span>' .$row[comments].'</p>
              ';
          };
          ?>
        </div>
      </div>
    </section>
    <footer>
      <p>Boardgame Diary, Copyright &copy; 2018</p>
    </footer>
  </body>
</html>
<?php
function get_post($conn, $var)
{
return $conn->real_escape_string($_POST[$var]);
}

$result->close();
$conn->close();
?>
