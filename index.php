<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content ="width=device-width">
    <meta name="author" content="Pavliko">
    <title>BoardgameDiary | Welcome</title>
    <link rel="stylesheet" href="./css/style.css">
    <script src="js/jquery-3.2.1.min.js"></script>
    <script src="js/script.js"></script>
  </head>
  <body>
    <header>
        <div id="logo">
          <h1>Board<span class="highlight">game</span>Diary</h1>
        </div>
        <nav>
            <a class="current" href="index.php">Home</a>
            <a href="diary.php">Diary</a>
            <a href="stats.php">Stats</a>
        </nav>
    </header>
    <section id="showcase">
      <div class="container">
        <h1>Your Own Boardgame Diary</h1>
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin iaculis est dolor, vitae egestas quam laoreet ultricies. Curabitur egestas vestibulum nunc pulvinar venenatis. Sed non nisi interdum, mollis velit vel, pharetra ante. </p>
      </div>
    </section>
    <section id="boxes">
        <div class="box">
          <img src="./img/list.png">
          <h3>Save your scores</h3>
        </div>
        <div class="box">
          <img src="./img/share.png">
          <h3>Share with friends</h3>
        </div>
        <div class="box">
          <img src="./img/file.png">
          <h3>Capture best moments with words</h3>
        </div>
    </section>
    <footer>
      <p>Boardgame Diary, Copyright &copy; 2018</p>
    </footer>
  </body>
</html>
